<?php

namespace AppBundle\Controller;

use AppBundle\Entity\PostsRepository;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $posts = $this->getDoctrine()->getRepository('AppBundle:Posts')->getLatestPosts(5);

        if(TRUE === $this->get('security.context')->isGranted('ROLE_USER')) {
            $loggedin = true;
        }
        else {
            $loggedin = false;
        }

        // replace this example code with whatever you need
        return $this->render('AppBundle::index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'pagetitle' => 'Home',
            'posts' => $posts,
            'loggedin' => $loggedin
        ));
    }

    /**
     * @Route("/posts/{hash}",name="post")
     */
    public function postsAction($hash)
    {
        $post = $this->getDoctrine()->getRepository('AppBundle:Posts')->getPost($hash);

        // replace this example code with whatever you need
        return $this->render('AppBundle::posts/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
            'post' => $post
        ));
    }

    /**
     * @Route("/generatepassword/{password}",name="generatepassword")
     */
    public function passwordAction($password)
    {
        $factory = $this->get('security.encoder_factory');
        // whatever *your* User object is
        $u = new User();

        $encoder = $factory->getEncoder($u);
        $password = $encoder->encodePassword($password, $u->getSalt());
        die($password);
    }

    /**
     * @Route("/users/{hash}",name="users page")
     */
    public function usersAction($hash)
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:Users')->getUserProfile($hash);
        return $this->render('AppBundle::users/profile.html.twig', array(
           'user' => $user
        ));
    }

    /**
     * @Route("/logout",name="logout")
     */
    public function logoutAction()
    {

    }

    /**
     * @Route("/account/",name="account")
     */
    public function accountIndexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        return $this->render('AppBundle::users/account.html.twig', array(
            'modals' => array()
        ));
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function adminIndexAction(Request $request)
    {
        die(print_r($request));
    }

    /**
     * @Route("/login", name="login_route")
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContextInterface::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        } else {
            $error = null;
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(SecurityContextInterface::LAST_USERNAME);

        return $this->render(
            'AppBundle::security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }
}
