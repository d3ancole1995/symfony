Symphony Blogger
====================

This is a personal project that I am doing to improve my knowledge of symfony & I thought I would share the source with the public

Requirements
====================

Symfony requires you to be running a minimum of PHP 5.4, You are advised to have caching and debugging plugins installed (personally I do, I use xDebug & opcache)

Setup
====================

I have the project installed on a VPS running CentOS 6.7. I use PHP 5.6.16 (installed using yum install php56w - after adding the epel repo), Apache (installed using yum install httpd) and lastly I have MySQL server 5.1 installed for the database side.

For setting up the project, I personally recommend you use the same setup, download the zip, unzip it and it should work (in theory). Of course you will need to setup a virtual hosts to point to the location of the extracted files. There is also the matter of sorting possible permission issues & you should also enable htaccess files (This can be done by changing the AllowOverride variable in your apache config to AllowOverride All)

Please be warned, I take no responsibility for the project causing you issues. It is not designed to be used in a 'public' environment & I am purley doing this just to expand my knowledge of Symfony. The system is under no circustances 'secure' although I will try to do my best to make it as good as possible. If I believe that this could be something good then I may or may not consider putting more effort into the project & attempt to make something of a reputation for it.

If you have any issues setting up then by all means contact me, but do not expect me to respond quick. I will respond as and when I can and also at my own discression. I am not providing a support service for this so don't expect me to connect remotley and sort it out.

Contact me using any of the following:
 - Email: d3ancole1995@gmail.com
 - Skype: fightapilotdean
 - Twitter: @fightapilotdean

Features
====================

###Current:
 - Pretty much nothing
 - Bootstrap

###Future
 - Authentication for users
 - Dashboard for writing posts (author)
 - Ability to sign up + comment on posts (normal user)
 - Dashboard for managing everything on the site (admin)
 - Embed content (images / video)
 - Share to social media (facebook / twitter - others in the future)
